#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>


void login();
void reg();
void createDB();


char ruserId, rpassword;


void login() {
	
	// 2b.
	
	int count;
    
    char userId, password, id, pass;
    
    printf("\n\t Input your username & password.  \n\n");
    
    printf("\n\t\t     Enter your Username :  ");
    scanf("%s", &userId);
    
    
    printf("\n\t\t     Enter your Password :  ");
    scanf("%s", &password);
    
    FILE *input;
    input = fopen("users.txt","r");
    
    fscanf(input,"%s",userId);
    fscanf(input,"%s",password);
    
    while(scanf(input, userId, password)) {
    	
    	if(strcmp(userId, id)==0 && strcmp(password, pass)==0 || strcmp(id, ruserId)==0 && strcmp(pass, rpassword)==0) {
            
            count = 1;
        }
        
        fclose(input);
        
        if(count == 1) {
            
            printf("\n\n\t____________________ Login Successful! _____________________\n\n\n");
            main();
        } else {
            printf("\n\n\t_______________________ Login ERROR ______________________\n\n\n");
            main();
        }
        
        break;
	}
	
	printf("\n\n\t____________________ Login Successful! _____________________\n\n\n");
}



void reg() {
	
	// 2a.
	
	FILE *input;
	input = fopen("users.txt","w");
	
	if(input == NULL) {
	    printf("Error");   
	    exit(1);             
	}
	
	printf("Enter username :   ");
	scanf("%s",&ruserId);
	printf("\n ");
	
	printf("\t\tEnter password :   ");
	scanf("%s",&rpassword);
	printf("\n ");
	
	fprintf(input,"%s",ruserId);
	
	fprintf(input," : ");
	
	fprintf(input,"%s \n",rpassword);
	
	fclose(input);
}



void createDB() {
	
	/* 2b. opens an existing csv file or creates a new file. */
	
	FILE *input;
	input = fopen("problems.tsv","w");
	
	int i, exit, n;
	char add[100], author[100], problem[1000];
	
	printf("Enter the no. of authors & problems :   ");
	scanf("%d",&n);
	printf("\n ");
	
	for (i = 0; i < n; i++) {
		
		
		
		/* 2c. ADD an author and problem to TSV file */
		
		printf("\n\t\t Enter the ADD command :   ");
		scanf("%s", &add);
		
		if (strcmp(add, "ADD")==0) {
			
			printf("\n\t\t Enter author name :   ");
			scanf("%s", &author);
			
			printf("\n\t\t Enter problem :   ");
			scanf("%s", &problem);
			
			while(add) {
				
				printf("\n Thank you! Review your problem in the given TSV file. \n");
				
				/* 2f. Submit problem */
				printf("\n\t\t Press 0 to SUBMIT & EXIT :   ");
				scanf("%d, &exit");
				
				break;
				
				if(exit == 0) {
					
					break;
				}
			}
		
		}

		/* 2d. Insert the data to the TSV file */
		fprintf(input,"\n\n %s",problem);
	
		fprintf(input," by ");
	
		fprintf(input,"%s",author);
	}
	
	
	fclose(input);
	    
}



int main() {
	
	int c;
    
    printf("\n\t|          Press 1 to LOGIN                | \n");
    
    printf("\n\t|          Press 2 to REGISTER             | \n");
    
    printf("\n\t|          Press 3 to ADD a Problem        | \n ");
    
    printf("\n\t|          Press 4 then 0 to EXIT          |\n ");
    
    /* printf("\t|          Press 5 to Download A File       |"); */
    
    printf("\n\n\n\t|     Press Enter your choice :  ");
    
    scanf("%d", &c);
    printf("\n\t\t");
    
    switch(c) {
    	
    	case 1:
            login();
            break;
        
        case 2:
            reg();
            break;
        
        case 3:
            createDB();
            break;
        
        case 4:
        	printf("\t\t\t______________________ Thank You ________________________\n\n\n");
        	return 0;
        
		/*
        case 5:
        	downLoad();
        	break;
        */
        
        default:
            printf("\n\t\t\t     Enter your choice :   ");
            main();
	}
}
