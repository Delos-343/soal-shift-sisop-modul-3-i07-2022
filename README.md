# soal-shift-sisop-modul-3-I07-2022
## **Laporan Resmi dan Penjelasan Soal Shift Modul 2**
### **Kelompok I-07**
**Anggota :**
- Selomita Zhafirah 5025201120
- Amelia Mumtazah Karimah 5025201128
- Mohammed Fachry Dwi Handoko 5025201159

## **Daftar Isi Lapres**
- [Soal-1](#soal1)
- [Soal-2](#soal2)
- [Soal-3](#soal3)

# Soal 1
## **1a**
This question is asking Novak to download 2 zip files in a different folder which are quote.zip and music.zip the unzip both of the files with thread.

```
void *unzipQuotes( void *ptr){
    char destination[101] = "/Users/selomitazhafiirah/soal1file/quote";
    char quotefold[101] = "/Users/selomitazhafiirah/file/quote.zip";
    char quote[101] = "/Users/selomitazhafiirah/soal1file/quote";

    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();

    strcpy(path, destination);
    strcat(path, "/quote");


    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldquo[] = {"mkdir","-p", quote, NULL};
        norepeatfork("/bin/mkdir", createfoldquo);

        char *unzipTheQ[] = {"unzip", "-q", quotefold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheQ);
    }
    else {
        while ((wait(&status))>0);
    }
    return NULL;
}

void *unzipMusic(void *ptr){
    char destination[101] = "/Users/selomitazhafiirah/soal1file/music";
    char musicfold[101] = "/Users/selomitazhafiirah/file/music.zip";
    char music[101] = "/Users/selomitazhafiirah/soal1file/music";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", music, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

        char *unzipTheM[] = {"unzip", "-q", musicfold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheM);
    }
    else {
        while ((wait(&status))>0);
    }
}
```
To unzip the files, I use unzip function from execv. then use char to store the destination folder of the zip. 

```
int thread;
    thread = pthread_create(&tid[0], NULL, unzipQuotes, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 0\n");
    
    thread = pthread_create(&tid[1], NULL, unzipMusic, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
```
On the main function, I use thread to run the function at the sampe time. I add conditional if else to know the read is success or not.

## **1b**

Here is the static const unsigned chard[] for decode.

```
static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66
    };
```

Then add function where the text will be decode using base 64.

```
int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }
 if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}
```
Here is the main function of base64 where the destination of file (quote.music).txt is assigned. This function is runned by thread to run at the same time.
```
void decodeBase64(char *name){
    DIR *dp;
    if ((dp = opendir(name)) != NULL){
        struct dirent **namelist;
        int n;

        n = scandir(name, &namelist, NULL, alphasort);
        int i = 0;
        if (n == -1)
            perror("scandir");

        for (int i = 0; i < n; i++){
            if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                char *decoded;
                char dirName[100];
                strcpy(dirName, name);
                strcat(dirName, "/");
                strcat(dirName, namelist[i]->d_name);
                free(namelist[i]);

                FILE *fp = fopen(dirName, "r");
                char line[100];
                while (fgets(line, 100, fp) != NULL){
                    char *in = line;
                    size_t inLen = strlen(in);
                    size_t outLen = inLen;
                    decoded = malloc(sizeof(char) * 32768);
                    base64decode(in, inLen, (unsigned char *)decoded, &outLen);
                }
                fclose(fp);

                char txtDir[100];
                strcpy(txtDir, name);
                strcat(txtDir, "/");
                strcat(txtDir, name);
                strcat(txtDir, ".txt");

                fp = fopen(txtDir, "a");
                fprintf(fp, "%s\n", decoded);
                fclose(fp);
            }
        }
        free(namelist);
        closedir(dp);
    }
}

void *decodeMainM(){
    if(pthread_equal(id, tid[2]))
        decodeBase64("music");
    else if(pthread_equal(id, tid[3]))
        decodeBase64("quote");
    return NULL;
}

}

```
## **1c**
Create a function to create a new directory folder named "hasil" using mkdir to create directory inside soal1 folder.
```
void *Createhasil(){
    char hasil[101] = "/Users/selomitazhafiirah/soal1file/hasil";
    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", hasil, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

    }
    else {
        while ((wait(&status))>0);
    }
}
```
move function is create to move folder using mv command. On the next function is the main function to move folder to folder hasil.
```
void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}
```
next function is the main function to move folder to folder hasil.
```
void *next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char hasil[101] = "/Users/selomitazhafiirah/soal1file/hasil";
        char quote[101] = "/Users/selomitazhafiirah/soal1file/quote/";
        char music[101] = "/Users/selomitazhafiirah/soal1file/music/";
        move(quote, hasil, "quote.txt");
        move(music, hasil, "music.txt");    
    }
    else{
        wait(NULL);
    }
}
```
## **1d**
make a zip function to make a name and password for password protection using zip command and read the password to protect the zip.
```
void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}
```
## **1e**
unzipWithpassword function is make to unzip hasil.zip first using that function (zip command)
```
void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}
```
make a deletedirectory function to delete old hasil directory for not duplicate the directory
```
void DeleteDirectory(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}
```
adding no.txt inside of hasil/no.txt
```
void addingNo(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}
```
Create zip main function to unzip/zip the directory with password.
```
void *zipMain(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        pid_t child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    DeleteDirectory("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addingNo("hasil/no.txt");
                        zip("hasil", password);
                        DeleteDirectory("hasil");
                    }
                }
            }
        }
    }
    return NULL;
}
```


# Soal 2
![lIbraries and utility functions](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/libs_and_voids.PNG)
* Standard and required libraries are imported based on the prerequisites of the module.
* Three utility functions are recalled beforehand to ensure that they get executed first (C : top-down).

![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/void_login.PNG)
* A login utility function is declared to allow users to log into the system based on available data (username, password).
* This is marked by the declaration of an integer 'count' to replace risky boolean variables during conditional comparisons.
* Basic inputs are assigned to the userId and password before opening a new file (exist ? read : mkdir) under the "user.txt" format.
* This is used to store user credentials for form validation - declared as 'input'.
* While the file is read and 'userId' and 'password' data are located, string-comparison conditionals are executed to crosscheck said
* data and 'count' is assigned a value of 1.
* Access to the file is then closed from the terminal.
* IF the user input matches the stored data, it will notify the user that login is successful, and return to the main menu.
* IF NOT, then notify the login failure and return to the main menu.
* Break the operation of the While loop and return to the main menu / clear screen either way.

![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/void_reg.PNG)
* User registration and data inputting => saving is done through this utility function.
* Declaration of character variables for ruserId, rpassword, rid, and rpass - the former two having space for 1000 characters.
* A file is opened / created to store the 'ruserId' and 'rpassword' data and the file is then overwritten.
* The data is then streamed into the file, then closed before returning to the main menu.

![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/void_createDB.PNG)
* Another utility function is created to create a simple database, formatted in 'problem.tsv' under the declaration 'input'.
* 'i', 'exit', and 'n', are declared as integer for indexing user inputs (counter).
* 'add', 'author', and 'problem' variables are declared as characters with a range of [100-1000].
* 'n' is dictated by the user for variables 'author' and 'problem'.

![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/void_createDB_for-loop.PNG)
* **REVISI 2c.2d.2f**
* A FOR loop reads through variable n, and assigns user inputs into them based on an ADD command.
* A string comparison conditional ensures that ADD is keyed in correctly to proceed and input 'author' and 'problem'.
* This is further developed with a WHILE loop holding the add variable, so that 'author' and 'problem' may be inputted under this condition.
* Once the latter two variables have been filled, users are given the option to SUBMIT their inputs with 'exit' = 0.
* The data for 'add', 'author' and 'problem' is then stored in the TSV file under the format : add, <problem> by author.
* Break the operation and return to the main menu + close the 'input' file.

![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/int_main.PNG)
* The main driver function is created last, pertaining to C's top-down processing of code.
* An integer for 'c', is declared to select the following options.
* The options is controlled by a switchcase method containing the void [utility] function recalls, represented as several test-cases.
* By default, however, the main driver function is recalled to prevent users from getting stuck in other sections.

# Output
![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/C_terminal.PNG)
![void login()](https://gitlab.com/Delos-343/soal-shift-sisop-modul-3-i07-2022/-/raw/Soal-2_README.md/blobs/img/output.PNG)
* Refer to the Demo G-drive for a recording of the full GUI and operations.


# Soal 3
## **3a**
Inti dari soal 3a adalah meminta Nami untuk mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif.

Langkah pertama, kami membuat beberapa deklarasi library untuk menjalankan program  dan integer.
```bash
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>
```
Kemudian membuat ```variabel``` agar dapat di proses selanjutnya.
```bash
pthread_t tid[800];
int count = 0;
char data[800][1000];
```


Karena terdapat larangan untuk tidak menggunakan  fork, exec dan system()(kecuali untuk bagian zip pada soal d). Maka untuk mengekstrak file dilakukan secara manual. Lalu setelah file sudah diekstrak langkah selanjutnya adalah mengkategorikan seluruh file pada working directory secara rekursif menggunakan ```bash
List File Recursively``` seperti berikut ini :
```bash
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";

void FileListingRecursively(char *basepath) {
    char path2[800];
    DIR *dir = opendir(basepath);
    struct dirent *dp;

    int n = 0

    if (!dir) {
        return;
    }
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {

            strcpy(path, basepath);
            strcat(path, dp->d_name);
            if (dp->d_type == DT_DIR) {
            	strcat(path, "/");	
            }
            char buffer[800];
            strcpy(buffer, dp->d_name);
            if (buffer[0] == '.') {

                createDir("Hidden");
                char Hidd[800];
                strcpy(Hidd, "Hidden/");
                strcat(Hidd, dp->d_name);
                rename(path2, Hidd);

            } else {
                char loc[800];
                strcpy(loc, pat2);
                
                strcpy(data[t1], loc);

                t1++;
            }
            FileListingRecursively(path2);
        }
    }
    closedir(dir);
}
```
Berdasarkan program diatas, kami melakukan read directory dan memasukan hasil lokasi direktori dari file/folder ke array data yang akan mengecek jika terdapat file/folder yang tersembunyi atau hidden. Untuk di awal loop, kamipun menambahkan kondisi if yang berfungsi untuk mengecek file yang di-read saat ini adalah sebuah directori atau folder, jika merupakan sebuah folder maka kami menambahkan /. 

## **3b**
Inti dari soal nomor 3b ini adalah meminta Nami memastikan  semua file harus berada di dalam folder, lalu jika terdapat file yang tidak memiliki ekstensi, maka file disimpan dalam folder “Unknown”. Dan sebaliknya jika terdapat file hidden, maka masuk folder “Hidden”.

Langkah selannjutnya,  melakukan pemotongan string pada nama file untuk mencari ekstensi dari file tersebut berdasarkan ```bash (.)``` pertama yang mencul. Jika terdapat (.) ada paling depan, maka akan masuk ke file Hidden, jika tidak ditemukan adanya tanda (.), maka akan masuk kedalam file unknown, dan jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”). Lalu, untuk mendapatkan nama file maka kami akan melakukan trim string. Kemudian membuat directory baru sesuai dengan ekstensi yang ada dan memindahkan file tersebut ke dalam folder yang sesuai ekstensinya.

```bash
void *move(void *filename)
 char cwd[path_max];
    char dirname[200], hidden[100], hiddenname[100], file[100], existsfile[100];
    int n;
    filelocation = (char *) location;
    DIR *dir = opendir(filelocation);

    if (dir == NULL) {
        strcpy(str1, filelocation);
        ext = strrchr(str1, '/');
        token = strtok(str1, ".");
        token = strtok(NULL, "");
        if (ext != NULL){

            if (ext[1] == "." && token != NULL) {
                strcpy(ext1, "Hidden");
            } else if (token == NULL) {
                strcpy(ext1, "Unknown");
            } else {
                strcpy(ext1, token);
              for (int n = 0; n < strlen(ext1); n++) {
                ext1[n] = tolower(ext1[n]);
                }
            }
        }
        
        strcpy(str2, fileloc);
        filename = strrchr(str2, '/');
        token = strtok(filename, "/");
        token = strtok(token, ".");
        token = strtok(token, "-");
        strcpy(fname, token);

        createDir(ext1);

        strcpy(fileloc2, fileloc);
        char temp[800];
        if (strcmp(ext1, "Unknown") == 0 || strcmp(ext1, "Hidden") == 0) {
            strcpy(temp, ext1);
            strcat(temp, "/");
            strcat(temp, fname);
        } else {
            strcpy(temp, ext1);
            strcat(temp, "/");
            strcat(temp, fname);
            strcat(temp, ".");
            strcat(temp, ext1);
             strcat(temp, "-");
            strcat(temp, ext1);
        }
        rename(filelocation2, temp);
    }
}
```

## **3c**
Inti dari soal nomor 3c ini adalah meminta Nami agar mengoperasikan thread disetiap kategori agar dapat mempercepat proses kategori.
```bash
int main() {
    char path[] = "/Users/amelia_mumtazah18/Documents/shift3/hartakarun";
    FileListingRecursively(path);

    for (int n = 0; n < t1; n++) {
        pthread_create(&(tid[n]), NULL, processFiles, (char*)data[n]);
    }
    for(int n = 0; n < t1; n++){
        pthread_join(tid[n],NULL);
    }
```


## **3d**
Inti dari soal nomor 3d ini adalah meminta Nami agar mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

Sesuai catatan dari soal bahwa Dilarang juga menggunakan  fork, exec dan system(),  kecuali untuk bagian zip pada soal d, maka kami menggunakan fork() dan exec() berikut programnya :
```bash
int main(){
    char path[] = "/Users/amelia_mumtazah18/Documents/shift3/hartakarun";
    zippingfile(path);

        void zippingfile(char *path){

            int condition;
            child_id = fork();

                 if (child_id == 0){
                 char *argv[] = {"zip", "-r", "hartakarun.zip", path, NULL};
                 execv("/usr/bin/zip", argv);

                } else{
                ((wait(&condition)) > 0);
            }
        }
    }
}
```


## **Output**
![3A](/uploads/bcf7fafabbb0e33b02d27059c074b28b/3A.png)
Pengkategorian file berhasil menggunakan command gcc -pthread -o -tes -soal3.c dilanjutkan ./tes. 
![3B](/uploads/56879a3808df5517fff1a4b01854b28d/3B.png)
![3D](/uploads/27c2cadea3b0c3c7bf32863ff6bd337c/3D.png)


